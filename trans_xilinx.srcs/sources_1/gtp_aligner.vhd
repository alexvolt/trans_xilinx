library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

--***********************************Entity Declaration************************

entity gtp_aligner is
generic(
	START_OF_PACKET_CHAR     : std_logic_vector ( 15 downto 0)  := x"027c");
port(
	txclk:						in std_logic;
	rxclk:						in std_logic;
	txrst:						in std_logic;
	rxrst:						in std_logic;	
	rx_byte_is_aligned:		in std_logic;
	rx_en_pcomma_det: 		out std_logic;
	isAligned:					out std_logic;
	txcharisk:					out std_logic_vector(1 downto 0);
	tx_data:						out std_logic_vector(15 downto 0));
end gtp_aligner;

architecture rtl of gtp_aligner is

signal tx_fsm_reset_done_r: std_logic;
signal tx_fsm_reset_done_r2: std_logic;
signal rx_byte_is_aligned_r: std_logic;
signal rx_byte_is_aligned_r2: std_logic;
signal isAligned_r: std_logic;
signal isAligned_r2: std_logic;
signal tx_data_counter: integer range 0 to 255;
signal isAligned_i: std_logic;
signal pattern_counter: integer range 0 to 63 := 0;

begin

isAligned <= isAligned_i;

-- si deve inviare ogni x dati un pattern per permettere sempre la resincronizzazione del ricevitore
process(txclk)
begin
	if rising_edge(txclk) then
		pattern_counter <= pattern_counter + 1;		-- quando torna a 0 invio un pattern, poi bisognerà metterlo come uno start of frame
	end if;
end process;

process(txclk)
begin
	if rising_edge(txclk) then
		if txrst = '1' then
			tx_data <= (others => '0');
			txcharisk <= "00";
			tx_data_counter <= 0;
		else
			if pattern_counter = 0 then 
				tx_data <= START_OF_PACKET_CHAR;
				txcharisk <= "01";
			else
				tx_data(7 downto 0) <= std_logic_vector(to_unsigned(tx_data_counter, 8));
				tx_data(15 downto 8) <= std_logic_vector(to_unsigned(tx_data_counter+1,8));
				txcharisk <= "00";
				tx_data_counter <= tx_data_counter + 2;
			end if;
		end if;
	end if;
end process;

process(rxclk)
begin
	if rising_edge(rxclk) then
		if rxrst = '1' then
			isAligned_i <= '0';
			rx_en_pcomma_det <= '0';
		else
			rx_byte_is_aligned_r <= rx_byte_is_aligned;
			rx_byte_is_aligned_r2 <= rx_byte_is_aligned_r;
			if rx_byte_is_aligned_r = '1' and rx_byte_is_aligned_r2 = '0' then
				isAligned_i <= '1';
			elsif rx_byte_is_aligned_r = '0' and rx_byte_is_aligned_r2 = '1' then
				isAligned_i <= '0';
			end if;
			if isAligned_i = '0' then
				rx_en_pcomma_det <= '1';
			else
				rx_en_pcomma_det <= '1';
			end if;
		end if;
	end if;
end process;

end rtl;
				
			